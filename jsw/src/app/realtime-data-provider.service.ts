import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class RealtimeDataProviderService {

  constructor(
    private http: HttpClient
  ) { }

  public selectedShellNotObservable = 1;
  setSelectedShellNotObservable(selectedShell: number) {
    this.selectedShellNotObservable = selectedShell;
    console.log('selected shell ', this.selectedShellNotObservable)
  }



  // add a parameter to decide which shell to send
  getShell(shellId: number) {
    return this.http.get('../assets/jswHeatSimulatedData_shell' + shellId + '.json');
  }

  getShell2() {
    return this.http.get('../assets/jswHeatSimulatedData_shell2.json')
  }

  getShell3() {
    return this.http.get('../assets/jswHeatSimulatedData_shell3.json')
  }

  getShell4() {
    return this.http.get('../assets/jswHeatSimulatedData_shell4.json')
  }


}
