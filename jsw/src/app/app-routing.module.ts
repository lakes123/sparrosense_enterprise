import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShellComponent } from './plant/shell/shell.component';
import { PlantComponent } from './plant/plant.component';
import { LiveViewComponent } from './live-view/live-view.component';
import { TrendsComponent } from './trends/trends.component';
import { ReportsComponent } from './reports/reports.component';
import { BusinessMetricsComponent } from './business-metrics/business-metrics.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { DataHubComponent } from './data-hub/data-hub.component';

const routes: Routes = [
  {path: 'shell', component: ShellComponent},
  {path: 'live-view', component: LiveViewComponent},
  {path: '', component: PlantComponent},
  {path: 'trends', component: TrendsComponent},
  {path: 'reports', component: ReportsComponent},
  {path: 'business-metrics', component: BusinessMetricsComponent},
  {path: 'analytics', component: AnalyticsComponent},
  {path: 'data', component: DataHubComponent},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
