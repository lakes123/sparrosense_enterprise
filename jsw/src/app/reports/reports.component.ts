import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  reportData = [];


  constructor() { }

  ngOnInit() {
    const shifts = ['A', 'B', 'D', 'C'];

    for (let i = 0; i < 4; i++) {
      const data = {
        heats: this.getRandomInt(12),
        td: this.getRandomInt(100) + 'm',
        ad: this.getRandomInt(10) + 1 + 'm',
        swmd: shifts[this.getRandomInt(3)],
        gwmd: this.getRandomInt(3) + 1

      };

      this.reportData.push(data);
    }
  }

  getRandomInt(max: number) {
    return Math.floor(Math.random() * Math.floor(max));
  }

}
