import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProcessContainerComponent } from './plant/shell/heat/process-container/process-container.component';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { ShellComponent } from './plant/shell/shell.component';
import { HeatComponent } from './plant/shell/heat/heat.component';
import { PlantComponent } from './plant/plant.component';
import { LiveViewComponent } from './live-view/live-view.component';
import { ProcessCumulativeComponent } from './plant/shell/heat/process-container-cumulative/process-cumulative.component';
import { TrendsComponent } from './trends/trends.component';
import { ReportsComponent } from './reports/reports.component';
import { BusinessMetricsComponent } from './business-metrics/business-metrics.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { DataHubComponent } from './data-hub/data-hub.component';

registerLocaleData(en);




@NgModule({
  declarations: [
    AppComponent,
    ProcessContainerComponent,
    ShellComponent,
    HeatComponent,
    PlantComponent,
    LiveViewComponent,
    ProcessCumulativeComponent,
    TrendsComponent,
    ReportsComponent,
    BusinessMetricsComponent,
    AnalyticsComponent,
    DataHubComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgZorroAntdModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
