export const shellProcessOptions = [
    {
        value: 'shell1',
        label: 'Shell 1',
        children: [
            {
                value: 'all',
                label: 'All processes',
                isLeaf: true
            },
            {
                value: 'ebj',
                label: 'End brick jam',
                isLeaf: true
            },
            {
                value: 'emfal',
                label: 'EBT mass filling and levelling ',
                isLeaf: true
            },
            {
                value: 'tlp',
                label: 'Top lance positioning',
                isLeaf: true
            },
            {
                value: 'li',
                label: 'Launder insertion',
                isLeaf: true
            },
            {
                value: 'hmp',
                label: 'HM positioning',
                isLeaf: true
            },
            {
                value: 'hmlcp',
                label: 'HM launder car pouring',
                isLeaf: true
            }
        ]
    },
    {
        value: 'shell2',
        label: 'Shell 2',
        children: [
            {
                value: 'all',
                label: 'All processes',
                isLeaf: true
            },
            {
                value: 'ebj',
                label: 'End brick jam',
                isLeaf: true
            },
            {
                value: 'emfal',
                label: 'EBT mass filling and levelling ',
                isLeaf: true
            },
            {
                value: 'tlp',
                label: 'Top lance positioning',
                isLeaf: true
            },
            {
                value: 'li',
                label: 'Launder insertion',
                isLeaf: true
            },
            {
                value: 'hmp',
                label: 'HM positioning',
                isLeaf: true
            },
            {
                value: 'hmlcp',
                label: 'HM launder car pouring',
                isLeaf: true
            }
        ]
    },
    {
        value: 'shell3',
        label: 'Shell 3',
        children: [
            {
                value: 'all',
                label: 'All processes',
                isLeaf: true
            },
            {
                value: 'ebj',
                label: 'End brick jam',
                isLeaf: true
            },
            {
                value: 'emfal',
                label: 'EBT mass filling and levelling ',
                isLeaf: true
            },
            {
                value: 'tlp',
                label: 'Top lance positioning',
                isLeaf: true
            },
            {
                value: 'li',
                label: 'Launder insertion',
                isLeaf: true
            },
            {
                value: 'hmp',
                label: 'HM positioning',
                isLeaf: true
            },
            {
                value: 'hmlcp',
                label: 'HM launder car pouring',
                isLeaf: true
            }
        ]
    },
    {
        value: 'shell4',
        label: 'Shell 4',
        children: [
            {
                value: 'all',
                label: 'All processes',
                isLeaf: true
            },
            {
                value: 'ebj',
                label: 'End brick jam',
                isLeaf: true
            },
            {
                value: 'emfal',
                label: 'EBT mass filling and levelling ',
                isLeaf: true
            },
            {
                value: 'tlp',
                label: 'Top lance positioning',
                isLeaf: true
            },
            {
                value: 'li',
                label: 'Launder insertion',
                isLeaf: true
            },
            {
                value: 'hmp',
                label: 'HM positioning',
                isLeaf: true
            },
            {
                value: 'hmlcp',
                label: 'HM launder car pouring',
                isLeaf: true
            }
        ]
    },
];





export const groupShiftOptions = [
    {
        value: 'group1',
        label: 'Group 1',
        children: [
            {
                value: 'shifta',
                label: 'Shift A',
                isLeaf: true
            },
            {
                value: 'shiftb',
                label: 'Shift B',
                isLeaf: true
            },
            {
                value: 'shiftc',
                label: 'Shift C',
                isLeaf: true
            },
            {
                value: 'shiftd',
                label: 'Shift D',
                isLeaf: true
            },
        ]
    },
    {
        value: 'group2',
        label: 'Group 2',
        children: [
            {
                value: 'shifta',
                label: 'Shift A',
                isLeaf: true
            },
            {
                value: 'shiftb',
                label: 'Shift B',
                isLeaf: true
            },
            {
                value: 'shiftc',
                label: 'Shift C',
                isLeaf: true
            },
            {
                value: 'shiftd',
                label: 'Shift D',
                isLeaf: true
            },
        ]
    },
    {
        value: 'group3',
        label: 'Group 3',
        children: [
            {
                value: 'shifta',
                label: 'Shift A',
                isLeaf: true
            },
            {
                value: 'shiftb',
                label: 'Shift B',
                isLeaf: true
            },
            {
                value: 'shiftc',
                label: 'Shift C',
                isLeaf: true
            },
            {
                value: 'shiftd',
                label: 'Shift D',
                isLeaf: true
            },
        ]
    },
    {
        value: 'group4',
        label: 'Group 4',
        children: [
            {
                value: 'shifta',
                label: 'Shift A',
                isLeaf: true
            },
            {
                value: 'shiftb',
                label: 'Shift B',
                isLeaf: true
            },
            {
                value: 'shiftc',
                label: 'Shift C',
                isLeaf: true
            },
            {
                value: 'shiftd',
                label: 'Shift D',
                isLeaf: true
            },
        ]
    },
];





export const delaysOptions = [
    {
        value: 'allDelays',
        label: 'All Delays',
        isLeaf: true,
    },
    {
        value: '<9m',
        label: '< 9 min.',
        children: [
            {
                value: 'all',
                label: 'All delays',
                isLeaf: true
            },
            {
                value: 'ebthr',
                label: 'EBT hopper refill',
                isLeaf: true
            },
            {
                value: 'md',
                label: 'Manpower delay',
                isLeaf: true
            },
            {
                value: 'ld',
                label: 'Levelling delay',
                isLeaf: true
            },
            {
                value: 'tlmd',
                label: 'Top lance movement delay',
                isLeaf: true
            },
            {
                value: 'lcd',
                label: 'Launder car delay',
                isLeaf: true
            },
            {
                value: 'cd',
                label: 'Crane delay',
                isLeaf: true
            },
            {
                value: 'hmd',
                label: 'HM delay',
                isLeaf: true
            },
            {
                value: 'mp',
                label: 'Misplanning',
                isLeaf: true
            },
            {
                value: 'spc',
                label: 'Slag pit cleaning',
                isLeaf: true
            },
        ]
    },
    {
        value: '9to12m',
        label: '9 to 12 min.',
        children: [
            {
                value: 'all',
                label: 'All delays',
                isLeaf: true
            },
            {
                value: 'ebthr',
                label: 'EBT hopper refill',
                isLeaf: true
            },
            {
                value: 'md',
                label: 'Manpower delay',
                isLeaf: true
            },
            {
                value: 'ld',
                label: 'Levelling delay',
                isLeaf: true
            },
            {
                value: 'tlmd',
                label: 'Top lance movement delay',
                isLeaf: true
            },
            {
                value: 'lcd',
                label: 'Launder car delay',
                isLeaf: true
            },
            {
                value: 'cd',
                label: 'Crane delay',
                isLeaf: true
            },
            {
                value: 'hmd',
                label: 'HM delay',
                isLeaf: true
            },
            {
                value: 'mp',
                label: 'Misplanning',
                isLeaf: true
            },
            {
                value: 'spc',
                label: 'Slag pit cleaning',
                isLeaf: true
            },
        ]
    },
    {
        value: '>9m',
        label: '> 12 min.',
        children: [
            {
                value: 'all',
                label: 'All delays',
                isLeaf: true
            },
            {
                value: 'ebthr',
                label: 'EBT hopper refill',
                isLeaf: true
            },
            {
                value: 'md',
                label: 'Manpower delay',
                isLeaf: true
            },
            {
                value: 'ld',
                label: 'Levelling delay',
                isLeaf: true
            },
            {
                value: 'tlmd',
                label: 'Top lance movement delay',
                isLeaf: true
            },
            {
                value: 'lcd',
                label: 'Launder car delay',
                isLeaf: true
            },
            {
                value: 'cd',
                label: 'Crane delay',
                isLeaf: true
            },
            {
                value: 'hmd',
                label: 'HM delay',
                isLeaf: true
            },
            {
                value: 'mp',
                label: 'Misplanning',
                isLeaf: true
            },
            {
                value: 'spc',
                label: 'Slag pit cleaning',
                isLeaf: true
            },
        ]
    },
];



export const gradeOptions = [
    {
        value: 'JDHALM550E',
        label: 'JDHALM550E',
        isLeaf: true
    },
    {
        value: 'JDHSH41ALN',
        label: 'JDHSH41ALN',
        isLeaf: true
    },
    {
        value: 'JDT0500DCN',
        label: 'JDT0500DCN',
        isLeaf: true
    },
    {
        value: 'JDHSH29ALN',
        label: 'JDHSH29ALN',
        isLeaf: true
    }
]