import { Component, OnInit } from '@angular/core';
import { shellProcessOptions, groupShiftOptions, delaysOptions, gradeOptions } from './filter-options';

declare var Chart: any;


@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  styleUrls: ['./trends.component.scss']
})
export class TrendsComponent implements OnInit {

  public shellProcessOptions = shellProcessOptions;
  public shellProcessValues: any[] = ['Shell 1', 'EBT mass filling and levelling'];

  public groupShiftOptions = groupShiftOptions;
  public groupShiftValues: any[] = ['Group 1', 'Shift A'];

  public delaysOptions: any[] = delaysOptions;
  public delaysValues: any[] = ['All delays'];

  public gradeOptions: any[] = gradeOptions;
  public gradeValues: any[] = ['JDHALM550E'];
  private dataToBeDisplayed;


  public chart = [];

  constructor() { }

  ngOnInit() {
    this.dataType = 'delay';
    this.drawChart();
  }


  drawChart() {
    const options = {
      responsive: true,
      maintainAspectRatio: false,
      title: {
        display: false,
        text: 'Average Delay',
        fontSize: 24,
        fontColor: '#000000',
        padding: 32,
        fontStyle: 'normal',
        fontFamily: "'Segoe UI', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif', 'Segoe UI Emoji'"
      },
      legend: {
        display: true,
        position: 'bottom',
        labels: {
          fontSize: 14,
          padding: 20,
          boxWidth: 40,
          fontFamily: "'Segoe UI', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif', 'Segoe UI Emoji'"
        }
      },
      layout: {
        padding: {
          left: 30,
          right: 40,
          top: 32,
          bottom: 20
        }
      },
      scales: {
        yAxes: [{

          ticks: {
            display: true,
            beginAtZero: true,
            max: 7,
          }
        }],
        xAxes: [{
          gridLines: {
            display: false,
          },
          ticks: {
            display: true,
            fontSize: 12,
            fontColor: '#000000'
          }
        }]
      },
    }

    this.chart = new Chart('trends-chart', {
      type: 'line',
      data: {
        labels: ['Jul 3', 'Jul 4', 'Jul 5', 'Jul 6', 'Jul 7'],
        datasets: [{
          data: [this.getRandomInt(2), this.getRandomInt(5), this.getRandomInt(6), this.getRandomInt(4), this.getRandomInt(5)],
          label: 'Average ' + this.dataType + ' (minutes)',
          borderColor: '#f3961d',
          borderWidth: 6,
          fill: false
        }]
      },
      options: options
    })
  }

  set dataType(dataType: string) {
    this.dataToBeDisplayed = dataType;
    this.drawChart();

  }

  get dataType(): string {
    return this.dataToBeDisplayed;
  }

  getRandomInt(max: number) {
    return Math.floor(Math.random() * Math.floor(max)) + 1;
  }
}