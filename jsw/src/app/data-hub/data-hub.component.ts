import { Component, OnInit } from '@angular/core';
import { shellProcessOptions, groupShiftOptions, delaysOptions, gradeOptions } from '../trends/filter-options';
import { RealtimeDataProviderService } from '../realtime-data-provider.service';


@Component({
  selector: 'app-data-hub',
  templateUrl: './data-hub.component.html',
  styleUrls: ['./data-hub.component.scss']
})
export class DataHubComponent implements OnInit {

  public shellProcessOptions = shellProcessOptions;
  public shellProcessValues: any[] = ['Shell 1', 'EBT mass filling and levelling'];

  public groupShiftOptions = groupShiftOptions;
  public groupShiftValues: any[] = ['Group 1', 'Shift A'];

  public delaysOptions: any[] = delaysOptions;
  public delaysValues: any[] = ['All delays'];

  public gradeOptions: any[] = gradeOptions;
  public gradeValues: any[] = ['JDHALM550E'];

  dataType = 'delay'

  public heats;
  public tabularData = [];

  private processNames: string[] = [
    'End brick jam',
    'EBT mass filling and levelling',
    'Top lance Positioning',
    'Launder Insertion',
    'HM positioning',
    'HM launder car pouring',
    'blowing'
  ]

  private grades = [
    'JDHALM550E',
    'JDHSH41ALN',
    'JDT0500DCN',
    'JDHSH29ALN',
  ]

  constructor(private rtService: RealtimeDataProviderService) { }

  ngOnInit() {
    this.rtService.getShell(1).subscribe(
      res => {
        this.heats = res;
        for (let i = 1; i < 50; i++) {
          this.tabularData.push({
            shell: 'Shell ' + (this.getRandomInt(4) + 1),
            id: this.heats[i].id,
            shellLife: this.getRandomInt(250) + 50,
            ebtLife: this.getRandomInt(150) + 50,
            grade: this.grades[this.getRandomInt(3)],
            process: this.processNames[(this.getRandomInt(5))],
            startTime: new Date(this.heats[i].startTime).toTimeString().slice(0, 8),
            endTime: new Date(this.heats[i].endTime).toTimeString().slice(0, 8)
          })
        }
      }
    );





  }

  getRandomInt(max: number) {
    return Math.floor(Math.random() * Math.floor(max));
  }

}
