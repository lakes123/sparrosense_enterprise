import { Component, OnInit, } from '@angular/core';
import { RealtimeDataProviderService } from '../../realtime-data-provider.service';
import { ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class ShellComponent implements OnInit {
  public heats: any;
  public currentHeat = 1;
  public onGoingHeat: number = null;
  public currentHeatData: any;
  public currentShell: number;

  // time
  public currentTime;

  // live-view
  public liveView = false;
  public liveViewbuttonType = 'secondary';



  constructor(
    private rtService: RealtimeDataProviderService,
    private cd: ChangeDetectorRef
  ) {

    this.currentShell = this.rtService.selectedShellNotObservable;

    this.rtService.getShell(this.currentShell).subscribe(
      heats => {
        this.doStuffOnInitAndChange(heats);
        this.currentHeat = this.findOnGoingHeat();
      }
    );
  }


  get thisTime() {
    return Date.now()
  }

  doStuffOnInitAndChange(heats) {
    this.heats = heats;
    this.onGoingHeat = this.findOnGoingHeat();
    this.currentHeatData = this.heats[this.currentHeat];
    this.cd.detectChanges();
  }


  ngOnInit() {
    this.currentTime = new Date().toTimeString().slice(0, 8);
    console.log('shell initialized or refreshed');

    this.currentShell = this.rtService.selectedShellNotObservable;

    this.currentShell = this.rtService.selectedShellNotObservable;
    this.rtService.getShell(this.currentShell).subscribe(
      heats => {
        this.doStuffOnInitAndChange(heats);
        this.currentHeat = this.findOnGoingHeat();
      }
    );

    // refresh if onGoing heat is currently in view
    const refreshHeatData = setInterval(() => {
      this.refreshHeat();
      this.cd.detectChanges();
    }, 2000);


  }


  nextHeat() {
    if (this.heats[this.currentHeat]['endTime'] < this.thisTime / 1000) {
      this.currentHeat++;
      this.currentHeatData = this.heats[this.currentHeat]
      console.log(this.thisTime / 1000, 'heat endTime ', this.heats[this.currentHeat].endTime);
    }
    else {
      console.log('this is the onGoing heat')
      this.onGoingHeat = this.currentHeat
    }
  }

  refreshHeat() {
    this.currentTime = new Date().toTimeString().slice(0, 8)

    this.rtService.getShell(this.currentShell).subscribe(
      heats => {
        this.doStuffOnInitAndChange(heats);
      }
    );

  }

  previousHeat() {
    if (this.currentHeat > 1) {
      this.currentHeat--;
      this.currentHeatData = this.heats[this.currentHeat]
    }
  }

  findOnGoingHeat(): number {
    let i = 1;
    while (this.heats[i].endTime < (this.thisTime / 1000)) {
      i++;
    }
    return i;
  }


  goToCurrentHeat(): void {
    this.currentHeat = this.findOnGoingHeat();
    this.refreshHeat();
  }


  toggleLiveView(): void {
    if (this.liveView === false) {
      this.liveViewbuttonType = 'primary';
      this.liveView = true;

    } else {
      this.liveViewbuttonType = 'secondary';
      this.liveView = false;

    }
  }

}
