import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-process-cumulative',
    templateUrl: './process-cumulative.component.html',
    styleUrls: ['./process-cumulative.component.scss']

})

export class ProcessCumulativeComponent implements OnInit {
    @Input() cumulative;
    @Input() showName: boolean;
    // showName = false;
    private scale = 20;

    public styleEndDelay

    public styleStartDelay

    public styleProcessTime

    public styleName;
    public name: string;

    constructor() {

    }

    ngOnInit() {
        this.styleEndDelay = {
            'width': this.cumulative.totalEndDelay / this.scale + '%'
        }

        this.styleStartDelay = {
            'width': this.cumulative.totalStartDelay / this.scale + '%'
        }

        this.styleProcessTime = {
            'width': this.cumulative.totalProcessTime / this.scale + '%'
        }

        if(this.showName === false){
            this.name  = ''
            this.styleName ={
                'width': '0'
            }

        }else{
            this.name = 'Cumulative'
        }
    }


}