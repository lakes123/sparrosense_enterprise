import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

@Component({
  selector: 'process-container',
  templateUrl: './process-container.component.html',
  styleUrls: ['./process-container.component.scss']
})
export class ProcessContainerComponent implements OnInit {

  @Input('process-data') process;

  @Input('process-duration') duration: number;

  @Input('show-process-names') showProcessNames: boolean;
  // public process: IprocessData = this.pProvider.getProcesses();

  // public process = {
  //   name: 'something',
  //   duration: 0,
  //   startPoint: 0,
  //   onGoing: false,
  //   sopTime: 88,
  //   timeTaken: 128, 
  //   startDelay: {
  //     duration: 0,
  //     onGoing: false,
  //   },
  //   endDelay: {
  //     duration: 0,
  //     onGoing: false
  //   }
  // }

  public styleStartPoint = {};
  public styleErrorStart = {};
  public styleErrorEnd = {};
  public styleProcess = {};
  public styleProcessName = {};
  public styleTimeTag = {};
  private scale = 30;

  public showLabels: boolean;


  constructor() { }

  ngOnChanges() {
    this.updateProcessDuration()
    this.showEndDelay()
    this.showStartDelay()
    this.displayProcessNames()
    this.styleStartPoint = {
      'width': this.process['startPoint'] / this.scale + '%'
    }

  }

  ngOnInit() {

    //initialize start point
    this.styleStartPoint = {
      'width': this.process['startPoint'] / this.scale + '%'
    }

    this.displayProcessNames();


    this.updateProcessDuration()
    this.showEndDelay()
    this.showStartDelay()


  }


  updateProcessDuration() {
    
    if (this.process['onGoing']) {
      this.styleProcess = {
        'width': this.process['duration'] / this.scale + '%',
        'background-color': '#7cbdff',
        'border-bottom-right-radius': '50px',
        'border-top-right-radius': '50px'
      };
    }
    if (!this.process['onGoing']) {
      this.styleProcess = {
        'width': this.process['duration'] / this.scale + '%',
        'background-color': '#7cbdff'
      };
    }
  }


  displayProcessNames() {
    if (this.showProcessNames) {
      this.showLabels = true;
      this.styleProcessName = {
        'width': '10%',
      }
      this.styleTimeTag = {
        'display': 'inline'
      }
    }

    if (!this.showProcessNames) {
      this.process['name'] = '';
      this.showLabels = false;
      this.styleProcessName = {
        'width': '0%',
      }
      this.styleTimeTag = {
        'display': 'none'
      }
    }
  }




  showEndDelay() {
    //if delay is current
    if (this.process['endDelay']['onGoing']) {
      this.styleErrorEnd = {
        'width': this.process['endDelay']['duration'] / this.scale + '%',
        'border-bottom-right-radius': '50px',
        'border-top-right-radius': '50px'
      }
    }

    //if there's an end delay
    if (this.process['endDelay']['duration'] > 0) {
      this.styleProcess = {
        'width': this.process['duration'] / this.scale + '%',
        'background-color': '#7cbdff',
        'border-bottom-right-radius': '0px',
        'border-top-right-radius': '0px'
      };
    }

    if (!this.process['endDelay']['onGoing']) {
      this.styleErrorEnd = {
        'width': this.process['endDelay']['duration'] / this.scale + '%'
      }
    }

    //hide border if there's no delay
    if (this.process['endDelay']['duration'] == 0) {
      this.styleErrorEnd = {
        'border': 'hidden',
      }
    }
  }


  showStartDelay() {
    //if delay is current
    if(this.process['startDelay']['duration'] < 0 ){
      this.styleErrorStart = {
        'border': 'hidden',
        'width': '0%'
      }
    }
    if (this.process['startDelay']['onGoing']) {
      this.styleErrorStart = {
        'width': this.process['startDelay']['duration'] / this.scale + '%',
        'border-bottom-right-radius': '50px',
        'border-top-right-radius': '50px'
      }
      //make sure that process isn't visible
      this.styleProcess = {
        'width': '0%',
      }
    }
    //if delay happened in the past
    if (!this.process['startDelay']['onGoing'] && this.process['startDelay']['duration'] > 0 ) {
      this.styleErrorStart = {
        'width': this.process['startDelay']['duration'] / this.scale + '%',
      }
    }

    //hide border if there's no delay
    if (this.process['startDelay']['duration'] == 0) {
      this.styleErrorStart = {
        'border': 'hidden',
      }
    }
  }









  incrementDuration() {
    this.process['duration'] = this.process['duration'] + 5
    // console.log(this.process['duration'])
    this.ngOnChanges()
  }

  toggleProcessOnGoing() {
    if (this.process['onGoing'] == true) {
      this.process['onGoing'] = false;
      this.ngOnChanges()
    }
    else {
      this.process['onGoing'] = true;
      this.ngOnChanges()
    }
  }


  //start delay
  incremenStartErrorDuration() {
    this.process['startDelay']['duration'] = this.process['startDelay']['duration'] + 5
    // console.log(this.process['startDelay']['duration'])
    this.ngOnChanges()
  }

  toggleStartDelayOnGoing() {
    if (this.process['startDelay']['onGoing'] == true) {
      this.process['startDelay']['onGoing'] = false;
      this.ngOnChanges()
    }
    else {
      this.process['startDelay']['onGoing'] = true;
      this.ngOnChanges()
    }
  }


  //end delay
  incremenEndErrorDuration() {
    this.process['endDelay']['duration'] = this.process['endDelay']['duration'] + 5
    // console.log(this.process['endDelay']['duration'])
    this.ngOnChanges()
  }

  toggleEndDelayOnGoing() {
    if (this.process['endDelay']['onGoing'] == true) {
      this.process['endDelay']['onGoing'] = false;
      this.ngOnChanges()
    }
    else {
      this.process['endDelay']['onGoing'] = true;
      this.ngOnChanges()
    }
  }

}
