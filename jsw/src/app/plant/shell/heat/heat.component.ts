import { Component, OnInit, Input, OnChanges, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-heat',
  templateUrl: './heat.component.html',
  styleUrls: ['./heat.component.scss'],
})
export class HeatComponent implements OnInit, OnChanges {

  @Input('heat-data') heat: any;
  @Input('show-labels') showLabels: boolean;

  public EBTMFAL;
  public EBJ;
  public HMLCP;
  public HMP;
  public LI;
  public TLP;
  public blowingProcess;
  public blowing: boolean;



  private processNames: string[] = [
    'End brick jam',
    'EBT mass filling and levelling',
    'Top lance Positioning',
    'Launder Insertion',
    'HM positioning',
    'HM launder car pouring',
    'blowing'
  ]


  constructor(private cd: ChangeDetectorRef) { }

  ngOnInit() {
  }

  ngOnChanges() {

    console.log('shell id: ', this.heat['shellId'])
    this.EBTMFAL = this.buildProcessObject('EBT mass filling and levelling');
    this.EBJ = this.buildProcessObject('End brick jam');
    this.HMLCP = this.buildProcessObject('HM launder car pouring');
    this.HMP = this.buildProcessObject('HM positioning');
    this.LI = this.buildProcessObject('Launder Insertion');
    this.TLP = this.buildProcessObject('Top lance Positioning');
    this.blowingProcess = this.buildProcessObject('blowing');
    console.log('refresh: ', 'blowing start delay: ', this.buildProcessObject('blowing').startDelay.duration);

    // console.log(this.buildProcessObject('blowing'))
    // console.log(this.buildProcessObject('HM launder car pouring'))
    // console.log(this.buildProcessObject('Top lance Positioning'))
    // console.log(this.buildProcessObject('Launder Insertion'))
    // console.log(this.buildProcessObject('End brick jam'))
    // console.log(this.buildProcessObject('EBT mass filling and levelling'))
    // console.log(this.buildProcessObject('HM positioning'))
    if (this.buildProcessObject('blowing').onGoing) {
      this.blowing = true;
    } else {
      this.blowing = false;
    }
  }

  get thisTime() {
    return Date.now();
  }

  get cumulative() {
    const cumulative = {
      totalProcessTime: 200,
      totalStartDelay: 600,
      totalEndDelay: 300
    }    
    return cumulative;
  }


  buildProcessObject(processName) {
    // console.warn('indexOf ', processName, 'is ', this.processNames.indexOf(processName))
    // console.warn('startPoint of ', processName, 'is ', this.getStartPoint(this.processNames.indexOf(processName)))
    const processData = {
      name: this.heat[processName]['name'],
      duration: this.getProcessDuration(processName),
      startPoint: this.getStartPoint(processName),
      startTime: this.toTimeHumanReadable(this.heat[processName].startTime),
      endTime: this.toTimeHumanReadable(this.heat[processName].endTime),
      totalDelay: this.heat[processName].totalDelay,

      onGoing: this.checkIfOnGoing(processName),
      sopTime: this.heat[processName]['timeTakenWODelay'],
      // difference of start and end
      timeTaken: (this.heat[processName].endTime - this.heat[processName].startTime) + this.getStartDelayDuration(processName),



      startDelay: {
        duration: this.getStartDelayDuration(processName),
        onGoing: this.startDelayOnGoing(processName),

        // startTime of heat + startPoint of process
        startTime: this.toTimeHumanReadable(this.heat.startTime + this.getStartPoint(processName)),

        endTime: this.toTimeHumanReadable(this.heat[processName].startTime)
      },



      endDelay: {
        duration: this.getEndDelayDuration(processName),
        onGoing: this.endDelayOnGoing(processName),
        // endTime of process - totalDelay of process
        startTime: this.toTimeHumanReadable(this.heat[processName].endTime - this.heat[processName].totalDelay),
        endTime: this.toTimeHumanReadable(this.heat[processName].endTime)
      },

    }
    return processData
  }



  // for process
  checkIfOnGoing(processName) {
    //starTime < date.now < endTime
    if (((this.thisTime / 1000) < this.heat[processName]['endTime'] - this.heat[processName]['totalDelay']) && (this.thisTime / 1000) > this.heat[processName]['startTime']) {
      return true
    }
    return false
  }

  getProcessDuration(processName): number {
    if (this.checkIfOnGoing(processName)) {
      return Math.floor(this.thisTime / 1000) - this.heat[processName]['startTime']
    }
    if (this.heat[processName]['endTime'] < Math.floor(this.thisTime / 1000)) {
      //timeTaken - totalDelay
      return (this.heat[processName]['endTime'] - this.heat[processName]['startTime']) - this.heat[processName]['totalDelay']
    }
    return 0

  }



  getEndDelayDuration(processName): number {
    const processNamesIndex = this.processNames.indexOf(processName)

    if (this.endDelayOnGoing(processName)) {
      return this.thisTime / 1000 - (this.heat[processName]['endTime'] - this.heat[processName]['totalDelay'])
    }
    if (!this.endDelayOnGoing(processName) && this.thisTime / 1000 > this.heat[processName]['endTime']) {
      return this.heat[processName]['totalDelay']
    }

    return 0;
  }



  getStartDelayDuration(processName): number {
    const processNamesIndex = this.processNames.indexOf(processName)
    if (processNamesIndex == 0) {
      return 0
    }
    if (this.startDelayOnGoing(processName)) {
      return this.thisTime / 1000 - this.heat[this.processNames[processNamesIndex - 1]]['endTime']
    }
    if (!this.startDelayOnGoing(processName) && this.thisTime / 1000 > this.heat[processName]['startTime']) {
      return this.heat[processName]['startTime'] - this.heat[this.processNames[processNamesIndex - 1]]['endTime']
    }

    return 0;
  }



  startDelayOnGoing(processName): boolean {
    const processNamesIndex = this.processNames.indexOf(processName)
    if (processNamesIndex == 0) { return false }
    // if current time is between previous process's endTime and this process's startTime
    if (this.thisTime / 1000 > this.heat[this.processNames[processNamesIndex - 1]]['endTime'] && this.thisTime / 1000 < this.heat[this.processNames[processNamesIndex]]['startTime']) {
      return true
    }
    return false
  }


  endDelayOnGoing(processName): boolean {
    //translates processName to index
    // const processNamesIndex = this.processNames.indexOf(processName)
    // if current time is between previous process's endTime and this process's startTime
    if ((this.thisTime / 1000 > this.heat[processName]['endTime'] - this.heat[processName]['totalDelay']) && (this.thisTime / 1000 < this.heat[processName]['endTime'])) {
      return true
    }
    return false
  }







  getStartPoint(processName): number {
    const processNamesIndex = this.processNames.indexOf(processName)
    if (processNamesIndex == 0) {
      return 0
    }
    else {
      return this.heat[this.processNames[processNamesIndex - 1]]['endTime'] - this.heat['startTime'];
    }
  }



  toTimeHumanReadable(date: number): string {
    let humanDate = new Date(date * 1000)
    let hours = humanDate.getHours()
    let minutes = humanDate.getMinutes()
    let seconds = humanDate.getSeconds()
    return hours + ':' + ('0' + minutes).substr(-2) + ':' + ('0' + seconds).substr(-2);
  }

}
