import { Component, OnInit } from '@angular/core';
import { RealtimeDataProviderService } from '../realtime-data-provider.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plant',
  templateUrl: './plant.component.html',
  styleUrls: ['./plant.component.scss']
})
export class PlantComponent implements OnInit {

  constructor(private rtProvider: RealtimeDataProviderService, private router: Router) { }
  public onGoingHeat1: any
  public onGoingHeat2: any
  public onGoingHeat3: any
  public onGoingHeat4: any

  ngOnInit() {
    this.rtProvider.getShell(1).subscribe(
      heats => {
        this.onGoingHeat1 = heats[this.findOnGoingHeat(heats)]
      }
    )

    this.rtProvider.getShell2().subscribe(
      heats => {
        this.onGoingHeat2 = heats[this.findOnGoingHeat(heats)]
      }
    )

    this.rtProvider.getShell3().subscribe(
      heats => {
        this.onGoingHeat3 = heats[this.findOnGoingHeat(heats)]
      }
    )

    this.rtProvider.getShell4().subscribe(
      heats => {
        this.onGoingHeat4 = heats[this.findOnGoingHeat(heats)]
      }
    )
  }

  findOnGoingHeat(heats): number {
    var i = 1
    while (heats[i]['endTime'] < (Date.now() / 1000)) {
      i++
    }
    return i
  }



  setSelectedShell(shellNumber) {
    this.rtProvider.setSelectedShellNotObservable(shellNumber);
    this.router.navigate(['shell']);
  }

}
