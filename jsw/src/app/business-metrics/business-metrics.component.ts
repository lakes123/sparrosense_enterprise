import { Component, OnInit } from '@angular/core';
declare var Chart: any;
declare var pattern: any;

@Component({
  selector: 'app-business-metrics',
  templateUrl: './business-metrics.component.html',
  styleUrls: ['./business-metrics.component.scss']
})
export class BusinessMetricsComponent implements OnInit {

  private delayReasons = [
    "HM unavailability",
    "Levelling delay",
    "Launder car delays",
    "Top lance delays",
    "Flapper not working",
    "Crane Delays",
  ]

  private dataSet = {
    labels: this.delayReasons,
    datasets: [{
      label: 'Revenue lost (in lacs)',
      backgroundColor: '#31bd8e70',
      borderWidth: 3,
      borderColor: '#14a37390',
      type: 'line',
      data: [
        this.getRandomInt(25) / 1.5,
        this.getRandomInt(25) / 1.5,
        this.getRandomInt(25) / 1.5,
        this.getRandomInt(25) / 1.5,
        this.getRandomInt(25) / 1.5,
        this.getRandomInt(25) / 1.5,
        this.getRandomInt(25) / 1.5
      ]
    }, {
      label: 'Delay (in percent)',
      backgroundColor: '#ff2e2e40',
      borderWidth: 3,
      borderColor: "#ff2e2e40",
      data: [
        this.getRandomInt(25),
        this.getRandomInt(25),
        this.getRandomInt(25),
        this.getRandomInt(25),
        this.getRandomInt(25),
        this.getRandomInt(25),
        this.getRandomInt(25)
      ]
    }, {
      label: 'No. of occurances',
      backgroundColor: '#5bcbf590',
      borderWidth: 3,
      borderColor: "#16a7de80",
      data: [
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2
      ]
    }, {
      label: 'Time lost',
      backgroundColor: '#ff94af80',
      borderWidth: 3,
      borderColor: '#eb608380',
      data: [
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2,
        this.getRandomInt(25) / 2
      ]
    }]

  }



  private chart = [];

  constructor() { }



  ngOnInit() {
    this.plotChart();
  }

  plotChart() {
    const options = {
      responsive: true,
      maintainAspectRatio: false,
      title: {
        display: true,
        text: 'Delays and revenue loss',
        fontSize: 24,
        fontColor: '#000000',
        padding: 32,
        fontStyle: 'normal',
        fontFamily: "'Segoe UI', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif', 'Segoe UI Emoji'"
      },
      legend: {
        display: true,
        position: 'bottom',
        labels: {
          fontSize: 14,
          padding: 20,
          boxWidth: 40,
          fontFamily: "'Segoe UI', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif', 'Segoe UI Emoji'"
        }
      },
      layout: {
        padding: {
          left: 30,
          right: 40,
          top: 32,
          bottom: 20
        }
      },
      scales: {
        yAxes: [{
          stacked: true,
          barPercentage: 0.7,
          gridLines: {
            display: false,
          },
          ticks: {
            display: false,
          }
        }],
        xAxes: [{
          stacked: true,
          gridLines: {
            display: false,
          },
          ticks: {
            display: true,
            fontSize: 12,
            fontColor: '#000000',
            max: 32,
            beginAtZero: true,
            fontFamily: "'Segoe UI', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif', 'Segoe UI Emoji'"
          }
        }]
      },
    }

    this.chart = new Chart('delay-reasons-chart', {
      type: 'bar',
      data: this.dataSet,
      options: options
    })
  }



  getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max)) + 1;
  }

}
